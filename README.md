
# pipewire-multiple-camera-feed-access-demo

A demo to showcase the capabilities of pipewire in letting multiple clients running in containers, access multiple cameras available in the system.

- [pipewire-multiple-camera-feed-access-demo](#pipewire-multiple-camera-feed-access-demo)
- [**Creating Container:**](#creating-container)
- [**Setting up pipewire:**](#setting-up-pipewire)
- [**Setting up wireplumber:**](#setting-up-wireplumber)
  - [Method 1](#method-1)
  - [Method 2](#method-2)

# **Creating Container:**

Podman is chosen as the container for this demo. First step is to install podman and then.

- Build Podman:
    
```
cd multiple-camera-feed-access-demo
podman build --tag debian-unstable-pipewire .
```
The Above build command places takes the dockerfile(included in the repositiory).

- Run Podman: 

```
podman run -it --rm -e WAYLAND_DISPLAY=wayland-0 \
                    -e XDG_RUNTIME_DIR=/run/user/0 \
                    -v /run/user/1000/pipewire-0:/run/user/0/pipewire-0:Z \
                    -v /run/user/1000/wayland-0:/run/user/0/wayland-0:Z \
                    -v /dev/dri:/dev/dri \
                    --security-opt label=disable \
                    -v ./aliases:/tmp/.bash_aliases:Z \
                    debian-unstable-pipewire /bin/bash --init-file /tmp/.bash_aliases
```

# **Setting up pipewire:**

- configure pipewire in restricted acces mode.
- stop pipewire deamon
```
systemctl --user stop pipewire.service  pipewire.socket  wireplumber.service  pipewire-pulse.service pipewire-pulse.socket
```
- start pipewire with new config file 
- restart the pipewire with custom conf file(the only diff is this conf file is the the pipewire access permissions).
```
pipewire -c <path>/pipewire.conf 2>&1 | tee ~/pipewire.log
```

# **Setting up wireplumber:**

## Method 1

- stop wireplumber demon(when u kill pipewire wireplumber goes down as well) 

> $ systemctl --user stop wireplumber.service

- Update the wireplumber.conf and start it from the source

```
cp wireplumber.conf <wireplumber src path>/wireplumber/src/config/
```

- Update the Lua config files
```
cp lua-policy-files/* <wireplumber src path>/src/scripts/
```
- run wireplumber from **source**
```
cd $HOME/code/pipewire/subprojects/wireplumber/
make run 2>$2 &
```

## Method 2

- apply wireplumber-0.4.5-22-gbe7b95c+cameras_demo.diff and then recompile and re-start the wireplumber. George has taken things in the Method 1 and then churned out Meathod 2