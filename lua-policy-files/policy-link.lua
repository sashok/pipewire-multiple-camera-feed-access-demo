linkables_om = ObjectManager {
  Interest {
    type = "SiLinkable",
    Constraint { "item.factory.name", "c", "si-audio-adapter", "si-node", type = "pw-global" },
  },
}

links_om = ObjectManager {
  Interest {
    type = "SiLink",
  }
}

function findTarget(node)
  local target_class_assoc = {
    ["Stream/Input/Audio"] = "Audio/Source",
    ["Stream/Output/Audio"] = "Audio/Sink",
    ["Stream/Input/Video"] = "Video/Source",
  }

  local target_media_class = target_class_assoc[node.properties["media.class"]]

-- check if node.target(camera preference) is set
  local node_target = node.properties["node.target"]
  if node_target then
    Log.info(node,"node.target is set to "..node_target)
    local node_target_found;
    
    for si in linkables_om:iterate() do
      local node = si:get_associated_proxy("node")
      Log.info(si,"node="..tostring(node).."node bound.id="..node["bound-id"])  

      if tostring(node["bound-id"]) == tostring(node_target) then
        node_target_found = true;
        Log.info(node,"node.target found")
        return si
      end

    end
    if not node_target_found then
      Log.info(node, "node.target="..node_target.." seems to be invalid"..
        "are you sure it points to right cam source node?")
    end
  end

-- pick any available cam source as no explicit preferece is set.
  for si in linkables_om:iterate() do
    local node = si:get_associated_proxy("node")
    if node.properties["media.class"] == target_media_class then
      return si
    end
  end
  return nil
end

function createLink(item, target)
  local node = item:get_associated_proxy ("node")
  local target_node = target:get_associated_proxy ("node")
  local media_class = node.properties["media.class"]
  local target_media_class = target_node.properties["media.class"]
  local out_item = nil
  local in_item = nil

  if string.find (media_class, "Input") or
     string.find (media_class, "Sink") then
    -- capture
    out_item = target
    in_item = item
  else
    -- playback
    out_item = item
    in_item = target
  end

  Log.info (string.format("link %s(%d) <-> %s(%d)",
      tostring(node.properties["node.name"]),
      node["bound-id"],
      tostring(target_node.properties["node.name"]),
      target_node["bound-id"]))

  -- create and configure link
  local link = SessionItem("si-standard-link")
  if not link:configure {
    ["out.item"] = out_item,
    ["in.item"] = in_item,
    ["out.item.port.context"] = "output",
    ["in.item.port.context"] = "input",
  } then
    Log.warning(link, "failed to configure si-standard-link")
    return
  end

  -- register & activate
  link:register()
  link:activate(Feature.SessionItem.ACTIVE, function (l, e)
    if e ~= nil then
      Log.warning (l, "failed to activate si-standard-link")
      link:remove ()
    else
      Log.info (l, "activated si-standard-link")
    end
  end)
end

linkables_om:connect("object-added", function(om, item)
  -- only handle session items that has a node associated proxy
  local node = item:get_associated_proxy("node")
  local media_class = node.properties["media.class"]
  Log.info (node, "handling linkable item " .. tostring(node.properties["node.name"]))

  -- check all the links to see if the item is already linked
  for link in links_om:iterate() do
    local out_id = tonumber(link.properties["out.item.id"])
    local in_id = tonumber(link.properties["in.item.id"])
    if out_id == item.id or in_id == item.id then
      -- already linked, nothing to do
      return
    end
  end

  local target = findTarget(node)
  if target then
    createLink(item, target)
  else
    Log.info("unable to findTarget for "..tostring(node));
  end
end)

linkables_om:connect("object-removed", function(om, item)
  -- remove any links associated with this item
  for link in links_om:iterate() do
    local out_id = tonumber (link.properties["out.item.id"])
    local in_id = tonumber (link.properties["in.item.id"])
    if out_id == item.id or in_id == item.id then
      link:remove ()
      Log.info(link, "link removed")
    end
  end
end)

links_om:activate()
linkables_om:activate()
