items = {}

remote_client_media_role = "Stream/Input/Video"

clients_om = ObjectManager {
  Interest { type = "client" }
}

function isNodeFromRemoteClient(node)
  local np = node.properties
  local media_class = np["media.class"]
  Log.info(node, "id is "..node["bound-id"].." and its media.class is "..media_class);

  if(string.find(media_class,"Stream")) then
    local client_id = np["client.id"]
    local client = clients_om:lookup { Constraint { "bound-id", "=", client_id, type = "gobject" }, }
    Log.info("client id is "..client_id.. tostring(client));
    local cp = client["properties"]
    if cp["application.process.host"] ~= Core.get_info()["host_name"] then
      return true
    end
  end    
end

function addItem (node, item_type)
  local id = node["bound-id"]
  local media_class = node.properties["media.class"]
  
  if isNodeFromRemoteClient(node) then
    if media_class ~= remote_client_media_role then
      Log.warning("remote client doesnt have priviledge to create session item");
      return
    end
  end

  -- create item
  items[id] = SessionItem ( item_type )

  if not items[id] then
    Log.warning("failed to create session item for node " .. tostring(id))
    return
  end

  -- configure item
  if not items[id]:configure {
      ["item.node"] = node,
      ["media.class"] = node.properties["media.class"],
  } then
    Log.warning(items[id], "failed to configure item for node " .. tostring(id))
    return
  end

  -- activate item
  items[id]:activate (Features.ALL, function(item)
    Log.info(item, "activated item for node " .. tostring(id))
    item:register()
  end)
end

nodes_om = ObjectManager {
  Interest {
    type = "node",
    Constraint { "media.class", "#", "Stream/*", type = "pw-global" },
  },
  Interest {
    type = "node",
    Constraint { "media.class", "#", "Video/*", type = "pw-global" },
  },
  Interest {
    type = "node",
    Constraint { "media.class", "#", "Audio/*", type = "pw-global" },
    -- Constraint { "wireplumber.is-endpoint", "-", type = "pw" },
  },
}

nodes_om:connect("object-added", function(om, node)
  local media_class = node.properties['media.class']
  Log.info(node,"object-added " .. media_class)
  if string.find (media_class, "Audio") then
    addItem(node, "si-audio-adapter")
  else
    addItem(node, "si-node")
  end
end)

nodes_om:connect("object-removed", function(om, node)
  local id = node["bound-id"]
  if items[id] then
    -- remove item from the global registry
    items[id]:remove()
    -- remove item from the local table
    items[id] = nil
  end
end)

nodes_om:activate()
clients_om:activate()