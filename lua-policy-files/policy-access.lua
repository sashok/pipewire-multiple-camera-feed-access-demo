clients_om = ObjectManager {
  Interest { type = "client" }
}

fact_om = ObjectManager {
  Interest { type = "factory"}
}

clients_om:connect("object-added", function(om, client)
    local id = client["bound-id"]
    local properties = client["properties"]

    if properties["application.process.host"] == Core.get_info()["host_name"] then
        -- Local client added grant full access
        client:update_permissions { ["any"] = "rwxm" }
      else
        Log.info(client, "remote client added"..tostring(client))
        -- first grant access to pw core
        client:update_permissions { [0] = "rwxm" }

        -- second grant access to client node factory, so that a stream node can be created.
        local cnf = fact_om:lookup { Constraint { "factory.name", "matches", "client-node", type = "pw-global" }, }
        if not cnf then
          Log.warning("access cannot be granted: unable to locate client node factory object");
          return
        end

        Log.info(cnf, "client node factory id ".. cnf["bound-id"])
        client:update_permissions { [cnf["bound-id"]] = "rwxm" }
      end
end)

fact_om:activate()
clients_om:activate()
