function createDevice(parent, id, type, factory, properties)
  -- set device name
  properties["device.name"] = "alsa_card." .. properties["device.bus-path"]

  -- set device description (user-friendly name)
  properties["device.description"] = properties["device.product.name"]

  Log.info("dev name="..properties["device.name"].."dev descp"..properties["device.description"].."factory is"..factory)
  local device = SpaDevice(factory, properties)
  device:connect("create-object", createNode)
  device:activate(Feature.SpaDevice.ENABLED | Feature.Proxy.BOUND)
  parent:store_managed_object(id, device)
end

function createNode(parent, id, type, factory, properties)
  -- setup node properties
  properties["device.id"] = parent["bound-id"]
  properties["factory.name"] = factory

  local device_properties = parent.properties

  local dev = properties["api.alsa.pcm.device"]
  local subdev = properties["api.alsa.pcm.subdevice"]
  local stream = properties["api.alsa.pcm.stream"]

  -- set node name
  properties["node.name"] =
      (stream == "capture" and "alsa_input" or "alsa_output")
      .. "." ..
      (device_properties["device.name"]:gsub("^alsa_card%.(.+)", "%1") or
       device_properties["device.name"] or
       "unnamed-device")
      .. "." ..
      (properties["device.profile.name"] or
       (stream .. "." .. dev .. "." .. subdev))

  -- set node description
  properties["node.description"] = device_properties["device.description"]

  -- create the node
  local node = Node("adapter", properties)
  node:activate(Feature.Proxy.BOUND)
  parent:store_managed_object(id, node)
end

monitor = SpaDevice("api.alsa.enum.udev", {})
monitor:connect("create-object", createDevice)
monitor:activate(Feature.SpaDevice.ENABLED)
